import { resourceModule } from '@reststate/vuex'

export default function ({ $axios, store }) {
  store.registerModule(
    'pizzas',
    resourceModule({ name: 'pizzas', httpClient: $axios })
  )
  store.registerModule(
    'orders',
    resourceModule({ name: 'orders', httpClient: $axios })
  )
  store.registerModule(
    'addresses',
    resourceModule({ name: 'addresses', httpClient: $axios })
  )
}
