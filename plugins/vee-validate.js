/* eslint-disable camelcase */
import Vue from 'vue'
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate'
import {
  required,
  email,
  alpha_num,
  integer,
  between,
  numeric,
  digits,
} from 'vee-validate/dist/rules'

extend('email', {
  ...email,
  message: 'Enter valid Email',
})

extend('required', {
  ...required,
  message: 'This field is required',
})

extend('alpha_num', {
  ...alpha_num,
  message: 'The field must only contain letters and numbers',
})

extend('digits', {
  ...digits,
  message: 'The field must contain of {length} digits',
})

extend('integer', {
  ...integer,
  message: 'The field must be an integer',
})

extend('between', {
  ...between,
  message: 'The field must be between {min} and {max}',
})

extend('numeric', {
  ...numeric,
  message: 'The field must be a number',
})

extend('password', {
  params: ['target'],
  validate(value, { target }) {
    return value === target
  },
  message: 'Passwords do not match',
})

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
