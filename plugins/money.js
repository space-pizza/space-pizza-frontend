import Vue from 'vue'

Vue.filter('money', function (value, currency) {
  if (!value) value = 0
  if (!(value instanceof Number)) value = parseFloat(value)

  return value.toFixed(2) + ' ' + currency
})
