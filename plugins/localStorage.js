import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'pizza',
    paths: ['cart', 'currency'],
  })(store)
}
