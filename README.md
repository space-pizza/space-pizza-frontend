# Space Pizza (Frontend)

> First Pizza delivery from the low orbit!

## About this project

This project is build for development purposes only, as test task for Innoscripta. Sadly enought, no pizza from space, my friend 😔

### Technologies used

- Vue.js + Nuxt.js
- Vuex
- Tailwind CSS
- Credential Management API. It makes you seamlessly login on checkout!
- Some third-party libraries (e.g. for persisting cart on reload)
- My personal favourite library - [`@reststate/vuex`](vuex.reststate.codingitwrong.com). It helps to connect JSON:API-backend to Vuex with no pain. ~3 years of using and still rocks!
- No lodash. Who needs it in age of ES6?

### Fun facts

- Tailwind's [`space-x`](https://tailwindcss.com/docs/space) class suits very nicely
- Dodo's images has been used. So if the purge their CDNs, it means no more tasty pizza images sill hurt you
- I've done all project on my weekends
- No capthca! Bots are welcome!
- Sorry, St., no Privacy Policy for you. And no Cookie Disclaimer. And it even doesn't ask for notifications. And newsletter. And signing app is optional. And no, we don't have any sales banner. Doesn't seems very 2020, right? 

## Build Setup

### Prerequesits

- Node.js LTS (Tested on Dubnium)
- [Backend API](https://gitlab.com/space-pizza/space-pizza-backend). Provide URL via enviroment variable (e.g. `API_URL=http://pizza-task-backend.test/api`)

### Startup script

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
