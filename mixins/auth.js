export default {
  methods: {
    login({ email, password }, remember = true) {
      return this.$auth
        .loginWith('default', { data: { email, password } })
        .then((response) => {
          if (remember) this.storePassword(email, password)
          return response
        })
    },

    register(data, login = true) {
      return this.$axios.post('auth/register', data).then(async (response) => {
        if (login) await this.login(data)
        return response
      })
    },

    logout() {
      this.$auth.logout()
    },

    storePassword(username, password) {
      if (!window && !window.PasswordCredential) {
        return Promise.resolve()
      }

      const cred = new window.PasswordCredential({
        id: username,
        password,
        name: username,
      })

      return navigator.credentials.store(cred)
    },

    async tryAutoSignIn() {
      if (this.$auth.loggedIn) return
      if (!window && !window.PasswordCredential) return

      const credential = await navigator.credentials.get({
        password: true,
        mediation: 'optional',
      })

      if (!credential) {
        return
      }

      return this.login({
        email: credential.id,
        password: credential.password,
      })
    },
  },
}
