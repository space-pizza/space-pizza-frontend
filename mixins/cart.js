import { mapGetters } from 'vuex'

export default {
  data: () => ({
    deliveryOption: 'delivery', // todo
    deliveryOptions: ['delivery', 'pickup'],
  }),

  computed: {
    vat() {
      const num = this.subtotal * 0.3

      return Math.round((num + Number.EPSILON) * 100) / 100
    },

    delivery() {
      return this.deliveryOption === 'delivery' ? 5 : 0
    },

    total() {
      const num = this.subtotal + this.vat + this.delivery

      return Math.round((num + Number.EPSILON) * 100) / 100
    },

    ...mapGetters('cart', ['totalQty', 'isEmpty', 'subtotal', 'items']),
    ...mapGetters('currency', ['selectedCurrency']),
  },
}
