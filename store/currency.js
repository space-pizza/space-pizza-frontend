export default {
  strict: false,

  state: () => ({
    selectedCurrency: 'USD',
  }),

  mutations: {
    SELECT_CURRENCY(state, currency) {
      state.selectedCurrency = currency
    },
  },

  actions: {
    selectCurrency({ commit }, currency) {
      commit('SELECT_CURRENCY', currency)
    },
  },

  getters: {
    selectedCurrency(state) {
      return state.selectedCurrency
    },
  },
}
