export default {
  strict: false,

  state: () => ({
    items: [],
  }),

  mutations: {
    ADD_TO_CART(state, data) {
      if (Array.isArray(state.items) && state.items.length) {
        const index = state.items.findIndex(
          ({ item: { id } }) => id === data.item.id
        )

        // change qty if found
        if (index !== -1) {
          const item = state.items[index]
          const qty = Math.max(item.qty + data.qty, 0)

          // remove, if set to 0
          if (qty < 1) state.items.splice(index, 1)
          else item.qty = qty

          return
        }
      }

      state.items.push(data)
    },
    SET(state, data) {
      state.items = data
    },
  },

  actions: {
    add({ commit }, data) {
      commit('ADD_TO_CART', data)
    },
    clear({ commit }) {
      commit('SET', [])
    },
  },

  getters: {
    items(state) {
      return state.items
    },

    qty(state) {
      return Array.isArray(state.items) ? state.items.length : 0
    },

    totalQty(state) {
      return Array.isArray(state.items)
        ? state.items.map((item) => item.qty).reduce((x, y) => x + y, 0)
        : 0
    },

    isEmpty(state, getters) {
      return getters.qty === 0
    },

    subtotal(state, gettets, rootState, rootGetters) {
      const currency = rootGetters['currency/selectedCurrency']

      const num = Array.isArray(state.items)
        ? state.items
            .map(({ item, qty }) => item.prices[currency].price * qty)
            .reduce((x, y) => x + y, 0)
        : 0

      return Math.round((num + Number.EPSILON) * 100) / 100
    },
  },
}
